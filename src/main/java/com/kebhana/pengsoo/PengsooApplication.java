package com.kebhana.pengsoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PengsooApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengsooApplication.class, args);
	}

}
