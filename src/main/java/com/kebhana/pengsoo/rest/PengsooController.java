package com.kebhana.pengsoo.rest;

import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kebhana.pengsoo.dao.UserDao;
import com.kebhana.pengsoo.dao.UserEventDao;
import com.kebhana.pengsoo.dao.AcctDao;
import com.kebhana.pengsoo.dao.PrdDao;
import com.kebhana.pengsoo.dao.StepDao;
import com.kebhana.pengsoo.model.User;
import com.kebhana.pengsoo.model.UserEvent;
import com.kebhana.pengsoo.model.Acct;
import com.kebhana.pengsoo.model.Prdt;
import com.kebhana.pengsoo.model.Step;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Pengsoo Service API")
@RestController
public class PengsooController {
	private String msgTemplate = "%s님  반갑습니다.";
	private final AtomicLong  vistorConouter = new AtomicLong();
	
	@Autowired
	private UserDao userDao;
	
	@ApiOperation(value="사용자 정보 가져오기")
	@RequestMapping(value="/user/{userId}", method=RequestMethod.GET)
	public ResponseEntity <User> getUserInfo(
			@PathVariable (name="userId", required = true) String userId
			) { 
		
		User re = null;
		
		try {
			log.info("Start select user_info");
			re = userDao.selectUser(userId);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<User> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="사용자 중단 정보 가져오기")
	@RequestMapping(value="/user/stop", method=RequestMethod.GET)
	public ResponseEntity <List<User>> getAcctList() { 
		
		List<User> list = null;
		try {
			log.info("Start select acct_info");
			list = userDao.selectUserStop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<User>> (list, HttpStatus.OK);
	}
	
	@ApiOperation(value="사용자 정보 변경하기")
	@RequestMapping(value="/user/{userId}", method=RequestMethod.PUT)
	public ResponseEntity <String > setUserUpdate(
			@PathVariable(name="userId",required = true ) String userId, 
			@RequestBody User user
		) throws Exception { 
		
		List<User> list = null;
		log.info("Start db update");
		user.setUserId(userId);
		int re  = userDao.updateUser(user);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	@Autowired
	private AcctDao acctDao;
	
	@ApiOperation(value="계좌 정보 가져오기")
	@RequestMapping(value="/acct/{userId}", method=RequestMethod.GET)
	public ResponseEntity <List<Acct>> getAcctList(
			@PathVariable (name="userId", required = true) String userId
			) { 
		
		List<Acct> list = null;
		try {
			log.info("Start select acct_info");
			list = acctDao.selectAcct(userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<Acct>> (list, HttpStatus.OK);
	}
	
	@ApiOperation(value="계좌 잔액 정보 가져오기")
	@RequestMapping(value="/acct/bal/{acctId}", method=RequestMethod.GET)
	public ResponseEntity <Acct> getAcctBal(
			@PathVariable (name="acctNo", required = true) String acctNo
			) { 
		
		Acct re = null;
		
		try {
			log.info("Start select user_info");
			re = acctDao.selectAcctBal(acctNo);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<Acct> (re, HttpStatus.OK);
	}
	
	@ApiOperation(value="계좌 정보 변경하기")
	@RequestMapping(value="/acct/{acctNo}", method=RequestMethod.PUT)
	public ResponseEntity <String > setAcctUpdate(
			@PathVariable(name="acctNo",required = true ) String acctNo, 
			@RequestBody Acct acct
		) throws Exception { 
		
		List<User> list = null;
		log.info("Start update acct_info");
		acct.setAcctNo(acctNo);
		int re  = acctDao.updateAcct(acct);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	@ApiOperation(value="계좌 정보 등록하기 ")
	@RequestMapping(value="/acct", method=RequestMethod.POST)
	public ResponseEntity <String > setAcctInsert(
			@RequestBody Acct acct 
			) throws Exception { 
		
		List<UserEvent> list = null;
		log.info("Start db insert");
		int re  = acctDao.insertAcct(acct);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	@Autowired
	private UserEventDao userEventDao;
	
	@ApiOperation(value="사용자 이벤트 등록하기 ")
	@RequestMapping(value="/event", method=RequestMethod.POST)
	public ResponseEntity <String > setUserEventInsert(
			@RequestBody UserEvent userEvent 
			) throws Exception { 
		
		List<UserEvent> list = null;
		log.info("Start db insert");
		int re  = userEventDao.insertUserEvent(userEvent);
		log.debug("result :"+ re);
		
		/*
		 * String stepId = userEvent.getStepId(); 
		 * String prdtId = userEvent.getPrdtId();
		 * String userId = userEvent.getUserId();
		 * 
		 * log.info("stepId : "+stepId); 
		 * log.info("prdtId : "+prdtId);
		 * log.info("userId : "+userId);
		 */
		
		if (userEvent.getStepId().equals("0401")) { // 가입완료
			Acct addacct = new Acct(); // 추가되는 계좌 (insert)
			Acct oriacct = new Acct(); // 기존 계좌 (update)
			Acct balacct = null; // 기존 계좌 잔액 정보 (select)
			
			addacct.setPrdtId(userEvent.getPrdtId());
			addacct.setUserId(userEvent.getUserId());
			
			Date today = new Date();
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd");
		    String date = DATE_FORMAT.format(today);
		    
		    //log.info("date :"+date);
		    //log.info(userEvent.getTrsfDate().toString());
			
			if (userEvent.getTrsfDate().toString().equals(date)) { // 이체일자가 오늘일 경우만 잔액에 추가
				//log.info("_test_");
				//oriacct.setBal(userEvent.getAmt());
				
				if (!userEvent.getPayAcctNo().equals("")) {
					//int oriacctre = acctDao.updateAcct(oriacct);
					
					addacct.setBal(userEvent.getAmt());

					balacct = acctDao.selectAcctBal(userEvent.getPayAcctNo()); // 기존 계좌 잔액 정보 (select)
					
					oriacct.setUserId(userEvent.getUserId());
					oriacct.setAcctNo(userEvent.getPayAcctNo());
					oriacct.setBal(balacct.getBal() - userEvent.getAmt());
					
					acctDao.updateAcct(oriacct); // 기존계좌 업데이트
				}
			}
			
			acctDao.insertAcct(addacct);
		}
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
}
