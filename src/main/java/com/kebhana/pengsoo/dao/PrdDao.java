package com.kebhana.pengsoo.dao;



import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.pengsoo.model.Prdt;



@Mapper
public interface PrdDao {

	/**
	 * 사용자 전체 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<Prdt> selectPrd() throws Exception;		
	
	int selectTest() throws Exception;		
}
			