package com.kebhana.pengsoo.dao;



import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.pengsoo.model.Acct;
import com.kebhana.pengsoo.model.User;



@Mapper
public interface UserDao {

	/**
	 * 사용자  정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	User selectUser(String userId) throws Exception;
	
	/**
	 * 사용자  중단 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<User> selectUserStop() throws Exception;	
	
	/**
	 * 사용자  정보 변경하기 
	 * @return
	 * @throws Exception
	 */
	int updateUser(User user) throws Exception;		
}
			