package com.kebhana.pengsoo.dao;



import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.pengsoo.model.Acct;
import com.kebhana.pengsoo.model.User;
import com.kebhana.pengsoo.model.UserEvent;



@Mapper
public interface AcctDao {

	/**
	 * 계좌 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<Acct> selectAcct(String userId) throws Exception;
	
	/**
	 * 계좌 잔액 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	Acct selectAcctBal(String acctNo) throws Exception;	
	
	/**
	 * 계좌 정보 변경하기
	 * @return
	 * @throws Exception
	 */
	int updateAcct(Acct acct) throws Exception;
	
	/**
	 * 계좌 정보 등록하기
	 * @return
	 * @throws Exception
	 */
	int insertAcct(Acct acct) throws Exception;
	
}
			