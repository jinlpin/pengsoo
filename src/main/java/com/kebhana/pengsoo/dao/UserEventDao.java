package com.kebhana.pengsoo.dao;



import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.pengsoo.model.User;
import com.kebhana.pengsoo.model.UserEvent;



@Mapper
public interface UserEventDao {

	/**
	 * 사용자 이벤트 저장하기 
	 * @return
	 * @throws Exception
	 */
	
	int insertUserEvent(UserEvent userEvent) throws Exception;
	
}