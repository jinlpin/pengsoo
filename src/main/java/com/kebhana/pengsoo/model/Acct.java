package com.kebhana.pengsoo.model;

import lombok.Data;

@Data
public class Acct {
	private String acctNo;
	private String prdtId;
	private String userId;
	private String prdtName;
	private Integer bal;
}
