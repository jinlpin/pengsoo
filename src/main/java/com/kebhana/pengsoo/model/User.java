package com.kebhana.pengsoo.model;

import lombok.Data;

@Data
public class User {
	private String userId;
	private String userNm;
	private String passWd;
	private String psnlInfoCollYn;
	private String mktYn;
	private String mktUseYn;
	private String prdtId;
	private String stepId;
	private String stepName;
	private String agree01;
	private String agree02;
	private String agree03;
	private String agree04;
	private String agree05;
	private Integer term;
	private String cycl;
	private Integer trsfDate;
	private Integer amt;
	private String payAcctNo;
	private String expiCnclDiv; 
	private String autoTrsfYn; 
	private String smsYn;

}