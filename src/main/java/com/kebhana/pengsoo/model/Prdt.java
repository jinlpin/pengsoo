package com.kebhana.pengsoo.model;

import lombok.Data;

@Data
public class Prdt {
	private String prdtId;
	private String prdtName;
	private String prdtDiv;
	private String prdtDesc;
	private Float intRt;
}
