package com.kebhana.pengsoo.model;

import lombok.Data;

@Data
public class UserEvent {
	private String bascDt;
	private String userId;
	private Integer eventSeq;
	private String prdtId;
	private String stepId;
	private String agree01;
	private String agree02;
	private String agree03;
	private String agree04;
	private String agree05;
	private Integer term;
	private String cycl;
	private Integer trsfDate;
	private Integer amt;
	private String payAcctNo;
	private String expiCnclDiv;
	private String autoTrsfYn;
	private String smsYn;
	private String bdatSendYn;
	private String bdatSendDtm;
	private String pushSendYn;
	private String pushSendDtm;
	private String haiSendYn;
	private String haiSendDtm;
	private String sysRegDtm;
}
