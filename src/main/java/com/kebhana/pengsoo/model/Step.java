package com.kebhana.pengsoo.model;

import lombok.Data;

@Data
public class Step {
	private String stepId;
	private String stepName;
	private String pushMsgDesc;
}
