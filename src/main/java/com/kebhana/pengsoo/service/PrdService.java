package com.kebhana.pengsoo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kebhana.pengsoo.model.Prdt;


@Service
public interface PrdService {
	/**
	 * 사용자 정보 가져오
	 * @return
	 */
	public List<Prdt> selectPrd();	
}
