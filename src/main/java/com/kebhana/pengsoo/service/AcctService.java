package com.kebhana.pengsoo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kebhana.pengsoo.model.Acct;


@Service
public interface AcctService {
	/**
	 * 사용자 정보 가져오
	 * @return
	 */
	public List<Acct> selectAcct();	
}
