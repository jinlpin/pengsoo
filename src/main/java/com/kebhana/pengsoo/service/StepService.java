package com.kebhana.pengsoo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kebhana.pengsoo.model.Step;


@Service
public interface StepService {
	/**
	 * 사용자 정보 가져오
	 * @return
	 */
	public List<Step> selectStep();	
}
